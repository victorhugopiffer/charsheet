+++
title = 'Skator Shield'
image = '/images/Skator_Shield.png'
date = 2024-10-13T03:25:03-03:00
draft = false
+++

**Defense**: 230
**Block**: 75%

# Origin
The shield carried by Skator warriors has a triangular shape, low weight, and medium dimensions, allowing good mobility for its bearer.