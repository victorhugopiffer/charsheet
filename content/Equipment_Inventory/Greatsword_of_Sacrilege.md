+++
title = 'GreatSword of Sacrilege'
image = '/images/GreatSword_of_Sacrilege.png'
date = 2024-10-13T03:09:17-03:00
draft = false
+++

**Attk**: 200
**Attk.Speed**: 0.75s
**Block**: 30%

# Special Hability

If the enemy's belief in gods, kings, or entities is strong enough, once the special ability is used, a powerful weakness will be inflicted, causing them to fall and making them unable to fight back for 5 seconds. Additionally, the wielder will steal 30% of the damage dealt with each hit as life.

# Origin
The Greatsword of Sacrilege is a unique item obtained after defeating [**Urlik the Usurper**](http://localhost:1313/Charsheet/characters/urlik_the_usurper/). It is known to be a legendary sword that can only be wielded by those whose will is stronger than a saint's faith, or by those whose wrath against idols drives their life toward destruction.
