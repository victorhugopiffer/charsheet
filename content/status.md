+++
title = 'Status'
date = 2024-10-12T17:39:22-03:00
draft = false
+++

# Title:

Moonlight Knight


# Current Situation:

Location - Dark Forest of Skator

After a long day searching for the antidote that promises to be the salvation for the Moonlight Princess's plague, you encountered the necromancer Lord Sigmore and his pack of undead wolves. During the battle, you managed to defeat all his minions, forcing him to flee. However, you sustained several lacerations, and your life now hangs in the balance. You must treat your wounds if you hope to survive.


# Status:

-Life: 252/2938
-Attk: 287/385
-Def: 306/577
-Stamina: 572/1085
-Agility: 89/142
-Magic: 1059/1059

# Weapon Proeficiecy:

## Specialization:
-GreatSword
-light-Medium shield

### Secundary Specialization:
-Buff/Debuff spells 

# God Blessing:

Hamlah' kiss

Moments before death, by the pitty of a god Hamlah ll appear and kiss you on the face. Your wounds ll cure, and no other harm ll touch your body during the next few moments.

Can be used only once per week.