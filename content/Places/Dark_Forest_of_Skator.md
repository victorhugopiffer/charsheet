+++
title = 'Dark Forest of Skator'
image = '/images/Dark_Forest_of_Skator.png'
date = 2024-10-13T18:36:51-03:00
draft = false
+++

The Skator's army resides here, alongside other mythical creatures such as fairies and centaurs. While the forest is rich in resources, its dangers have caused adventurers to mantain distance.