+++
title = 'Sinteryl Kingdom'
image = '/images/Sinteryl_Kingdom.png'
date = 2024-10-13T18:20:27-03:00
draft = false
+++

In the Farlands of the continent, prevail the Kingdom of Sinteryl, also known as The Moonlight Kingdom. The power of [**King Aufred II**](http://localhost:1313/Charsheet/Characters/King_Aufred_II) extends all the domains from the [**grassy fields**](http://localhost:1313/Charsheet/Places/Grassy_Fields) to the [**DarkForest of Skator**](http://localhost:1313/Charsheet/places/dark_forest_of_skator/). The limits of its power resides on the emergence of [**Skator Kingdom**](http://localhost:1313/Charsheet/places/skator_kingdom), chiefed by [**Carthus Kalef**](http://localhost:1313/Charsheet/characters/chartus_kalef/), an undead that aims the destruction and domination of Sinteryl.