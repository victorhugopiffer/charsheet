+++
title = 'Grassy Fields'
image = '/images/Grassy_Fields.png'
date = 2024-10-13T18:36:16-03:00
draft = false
+++

Composed of expansive light green fields, this region falls under the domain of the [**Kingdom of Sinteryl**](http://localhost:1313/Charsheet/places/sinteryl_kingdom/) and is inhabited by its people. Although it benefits from the protection of Sinteryl's army, the area often suffers from rebel attacks.