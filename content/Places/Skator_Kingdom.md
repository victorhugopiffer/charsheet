+++
title = 'Skator Kingdom'
image = '/images/Skator_Kingdom.png'
date = 2024-10-13T18:37:16-03:00
draft = false
+++

Once the largest kingdom on the continent, it has faced a dramatic downfall due to the relentless wars waged by [**Chartus Kalef**](http://localhost:1313/Charsheet/Characters/Chartus_Kalef). The ruins of the grand castle now lie in the hands of Skator's army of undead, following the mass resurrection orchestrated by the necromancer [**Lord Sigmore**](http://localhost:1313/Charsheet/Characters/Lord_Sigmore).