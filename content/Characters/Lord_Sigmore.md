+++
title = 'Lord Sigmore'
image = '/images/Lord_Sigmore.png'
date = 2024-10-13T19:52:40-03:00
draft = false
+++

Lord Sigmore was banished from the [**Kingdom of Sinteryl**](http://localhost:1313/Charsheet/places/sinteryl_kingdom/) for reasons unknown. Fueled by his hatred for King Aufred, he resurrected [**Chartus Kalef**](http://localhost:1313/Charsheet/characters/chartus_kalef/), seeking the downfall of Sinteryl. As a powerful necromancer, Sigmore possesses the ability to reanimate fallen soldiers and beasts to fight alongside him.