+++
title = 'Urlik the Usurper'
image = '/images/Urlik_the_Usurper.png'
date = 2024-10-13T19:08:48-03:00
draft = false
+++

Urlick the Usurper, the general of Skator's army, earned his title after killing his predecessor and claiming the position for himself. Like most of Skator’s forces, Urlick was undead, but unlike the others, he was granted a blessing by the Sacrilege God. This divine favor bestowed upon him extraordinary powers and a legendary weapon known as the [**Greatsword of Sacrilege**](http://localhost:1313/Charsheet/equipment_inventory/greatsword_of_sacrilege/).