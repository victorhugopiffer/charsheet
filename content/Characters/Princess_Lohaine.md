+++
title = 'Princess Lohaine'
image = '/images/Princess_Lohaine.png'
date = 2024-10-13T18:34:59-03:00
draft = false
+++

Princess Lohaine, daughter of [**King Aufred II**](http://localhost:1313/Charsheet/Characters/King_Aufred_II), is a charming young woman in her twenties. Raised with the finest royal education, she mastered many skills, including playing the harp, her favorite instrument and hobby. However, she suddenly contracted a mysterious illness that left her body frail, covered in painful black blotches and blisters. Now, she spends most of her time bedridden, hoping her father will succeed in finding a cure.