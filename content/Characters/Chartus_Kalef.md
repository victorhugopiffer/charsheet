+++
title = 'Chartus Kalef'
image = '/images/Chartus_Kalef.png'

date = 2024-10-13T18:35:57-03:00
draft = false
+++


Many years ago, Chartus Kalef was a tyrannical king who ruled his people with an iron fist. After waging several wars against neighboring kingdoms, he was ultimately defeated and killed in a great battle on the Grassy Fields. Decades passed until a necromancer named [**Lord Sigmore**](http://localhost:1313/Charsheet/characters/lord_sigmore/) stumbled upon Kalef’s remains. Alongside his disciples, Sigmore performed a dark ritual to resurrect the fallen king. The ritual succeeded, but Kalef’s body was no longer flesh and blood—he returned as a mere skeleton. Despite this ghastly form, Chartus could still speak, think, and fight, now stronger than ever before. Determined to regain the glory he once held, he sets out to reclaim his former power.
