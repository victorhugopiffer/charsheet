+++
title = 'King Aufred II'
image = '/images/King_Aufred_II.png'
date = 2024-10-13T18:35:22-03:00
draft = false
+++

Son of Aufred I, this king is none other than the most powerful man on the entire continent. His ambition led him to conquer smaller kingdoms, but his compassion earned him fame and adoration from many. Despite all his power, his only daughter, [**Princess Lohaine**](http://localhost:1313/Charsheet/characters/princess_lohaine/), suffers from a mysterious plague that emerged not long ago. Now, he devotes all his energy and resources to finding a cure, though some say none exists.